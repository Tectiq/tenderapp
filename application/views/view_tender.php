<blockquote style="margin: unset;">
    Tender Information
</blockquote>
<div class="row">
    <div class="col s12 m12">
        <div class="card blue-grey darken-1">
            <div class="card-content white-text">
                <span class="card-title"><?php echo $item->Work_Name; ?></span>
                <div class="row">
                    <div class="col s6 m6">
                        Work order no:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Work_Order_No; ?>
                    </div>
                    <div class="col s6 m6">
                        Transaction Id:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Transaction_Id; ?>
                    </div>
                    <div class="col s6 m6">
                        Prabhag:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Prabhag; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<blockquote style="margin: unset;">
    Before work angles
</blockquote>
<div class="row">
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'Before', 1)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">1</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('1','Before')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">1</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'Before', 2)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">2</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('2','Before')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">2</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'Before', 3)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">3</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('3','Before')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">3</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'Before', 4)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">4</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('4','Before')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">4</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<blockquote style="margin: unset;">
    Work progress based on Activity
</blockquote>
<?php foreach ($this->main->get_activity_list($item->Transaction_Id) as $val): ?>
    <div class="row">
        <div class="col s9 m9">
            <div class="card" style="height: 50px;">
                <div class="card-content black-text" style="padding: 10px;">
                <span><?php echo $val->Activity_Name; ?>
                </div>
            </div>
        </div>
        <div class="col s3 m3">
            <a class="btn-floating btn-large waves-effect waves-light light-blue lighten-1" onclick="open_modal('<?php echo $val->Id ?>','Progress')"><i
                        class="material-icons">add</i></a>
        </div>
    </div>
<?php endforeach; ?>
<blockquote style="margin: unset;">
    After work angles
</blockquote>
<div class="row">
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'After', 1)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">1</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('1','After')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">1</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'After', 2)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">2</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('2','After')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">2</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'After', 3)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">3</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('3','After')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">3</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col s3 m3">
        <?php if ($this->main->check_angle($item->Work_Order_No, 'After', 4)): ?>
            <div class="card green" style="height: 70px;border-radius: 50%;">
                <div class="card-content white-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">4</span></center>
                </div>
            </div>
        <?php else: ?>
            <div class="card yellow" style="height: 70px;border-radius: 50%;" onclick="open_modal('4','After')">
                <div class="card-content blue-grey-text" style="padding: unset">
                    <br>
                    <center><span class="card-title">4</span></center>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<div id="modal1" class="modal modal-fixed-footer">
    <form method="post" action="<?php echo base_url;?>save_tender" enctype="multipart/form-data" id="info_form">
        <div class="modal-content">
            <p class="amber-text">Capture Photo & Location</p>
            <a class="btn" onclick="getLocation()" id="get_location">Get Location</a>
            <div id="loc_details">
                <p>
                    <strong>Latitude:</strong><input type="text" name="lat" id="lat" readonly required>
                    <br>
                    <strong>Longitude:</strong><input type="text" name="lon" id="longi" required readonly>
                </p>
            </div>
            <input type="hidden" name="ang" id="ang" value="">
            <input type="hidden" name="status" id="status" value="">
            <input type="hidden" name="wo" id="wo" value="<?php echo $item->Work_Order_No; ?>">
            <input type="file" name="img" id="mypic"  accept="image/*" capture="camera" required>
            <canvas width="100%" style="width: 100%;height: 150px"></canvas>
        </div>
        <div class="modal-footer">
            <p id="msg" class="red-text" style="float: left"></p>
            <button type="button" id="save_info" name="save_info" class="waves-effect waves-green amber btn" style="float: right">Save</button>
        </div>
    </form>
</div>

<script>
    $('#msg').hide();
    function open_modal(angle,status) {
        $('#ang').val(angle);
        $('#status').val(status);
        $('#modal1').modal('open');
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert('Device not supported for location');
        }
    }

    $("#save_info").click(function() {
        var lat = $('#lat').val();
        var lon = $('#longi').val();
        var img = $('#mypic').val();
        var ang = $('#ang').val();
        var status = $('#status').val();
        var msg = $('#msg');
        msg.hide();
        var wo = '<?php echo $item->Work_Order_No; ?>';
        if(lat != "" && lon != "" && img != "" && ang != "" && status != ""){
            $('#info_form').submit();
        }else{
            msg.show();
            msg.text('Location & Photo is mandatory');
        }
    });

    function getBase64(file) {
        var reader = new FileReader();
        var data;
        reader.readAsDataURL(file);
        reader.onload = function () {
            $('#imgsrc').val(reader.result);
            console.log(reader.result);
        };
    }

    function showPosition(position) {
        $('#lat').val(position.coords.latitude);
        $('#longi').val(position.coords.longitude);
    }

    var input = document.querySelector('input[type=file]'); // see Example 4
    input.onchange = function () {
        var file = input.files[0];
        //upload(file);
        drawOnCanvas(file);   // see Example 6
        //displayAsImage(file); // see Example 7
    };
    function drawOnCanvas(file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var dataURL = e.target.result,
                c = document.querySelector('canvas'), // see Example 4
                ctx = c.getContext('2d'),
                img = new Image();

            img.onload = function() {
                c.width = img.width;
                c.height = img.height;
                ctx.drawImage(img, 0, 0);
            };

            img.src = dataURL;
        };

        reader.readAsDataURL(file);
    }
</script>
