
<footer class="page-footer orange">
    <div class="footer-copyright">
        <div class="container">
            Developed by <a class="orange-text text-lighten-3" href="https://pursullence.com/">Pursullence</a>
        </div>
    </div>
</footer>


<!--  Scripts-->

<script src="<?php echo assets_url;?>js/materialize.js"></script>
<script src="<?php echo assets_url;?>js/init.js"></script>
<script>
    M.AutoInit();
    $('.modal').modal();
</script>
</body>
</html>
