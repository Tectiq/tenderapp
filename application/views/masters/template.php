<?php $this->load->view('masters/head'); ?>

<body>
<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper">
        <a id="logo-container" href="#" class="brand-logo">TPMS</a>
        <?php
            if($this->session->userdata('Email') != ""):
        ?>
            <a href="<?php echo base_url;?>logout" class="sidenav-trigger"><i class="material-icons">power_settings_new</i></a>
        <?php endif; ?>
    </div>

</nav>

<div>
    <div class="section" style="min-height: 100vh;">
        <?php $this->load->view($content); ?>
    </div>
</div>

<?php $this->load->view('masters/footer'); ?>
