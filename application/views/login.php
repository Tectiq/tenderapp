<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
    <div class="col s12 m6">
        <div class="card">
            <div class="card-content white-text">
                <form action="" method="POST">
                <span class="card-title blue-grey-text">Login</span>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" name="email" type="text" class="validate" required>
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="password" name="password" type="password" class="validate" required>
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col s12">
                        <center><button name="login" class="waves-effect waves-light btn">LOGIN</button></center>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
