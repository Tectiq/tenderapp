<?php foreach ($this->main->get_tender() as $item):?>
<div class="row">
    <div class="col s12 m6">
        <div class="card blue-grey darken-1">
            <div class="card-content white-text">
                <span class="card-title"><?php echo $item->Work_Name;?></span>
                <div class="row">
                    <div class="col s6 m6">
                        Work order no:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Work_Order_No;?>
                    </div>
                    <div class="col s6 m6">
                        Transaction Id:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Transaction_Id;?>
                    </div>
                    <div class="col s6 m6">
                        Prabhag:
                    </div>
                    <div class="col s6 m6">
                        <?php echo $item->Prabhag;?>
                    </div>
                </div>
            </div>
            <div class="card-action">
                <a href="<?php echo base_url;?>view-tender/<?php echo $item->Work_Order_No;?>">View</a>
<!--                <span class="white-text">Progress --><?php //echo $this->main->get_geo_progress($item->Work_Order_No);?><!--%</span>-->
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>