<?php

class Main extends CI_Model
{
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('encryption');
        $this->encryption->initialize(array('driver' => 'mcrypt'));
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function get_tender($tender_id = ''){
        if($tender_id != "")
            $this->db->where('Work_Order_No',$tender_id);
        else
            $this->db->where('Work_Order_No !=','');

        $this->db->where("Utility_Certificate is null");
        $this->db->where("Department_Id",$this->session->userdata('Department_Id'));
        $this->db->order_by('Created_On','DESC');
        $q = $this->db->get("tenderRecords");
        return $q->result();
    }

    public function get_geo_progress($tender_id){
        $this->db->select('Max(Progress) as Progress');
        $this->db->where('Work_Order_No',$tender_id);
        $q = $this->db->get("geoRecords");
        return $q->result()[0]->Progress;
    }

    public function check_angle($wo,$status,$angles){
        $this->db->where('Status',$status);
        $this->db->where('Angles',$angles);
        $this->db->where('Work_Order_No',$wo);
        $q = $this->db->get("geoRecords");
        if($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function get_activity_list($trans_id){
        $this->db->where('Transaction_Id',$trans_id);
        $this->db->group_by('Activity_Name');
        $this->db->order_by('Id','ASC');
        $q = $this->db->get("activity");
        return $q->result();
    }

}