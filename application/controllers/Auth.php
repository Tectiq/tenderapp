<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('main');
        $this->load->database();
        $this->load->library('support');
        if($this->session->userdata('Email') != ""){
            echo '<script>window.location.replace("'.base_url.'")</script>';
        }
    }

    public function index()
    {
        if(isset($_POST['login'])){
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $data = array('Email'=>$email,'Password' =>$password,'Role'=>'3');
            if($this->support->login($data))
                echo '<script>window.location.replace("'.base_url.'")</script>';
            else{
                echo '<script>alert("Wrong Email and Password");</script>';
            }
        }
        $data['content'] = 'login';
        $this->load->view('masters/template',$data);
    }

    public function logout(){
        $this->support->logout();
    }

}
