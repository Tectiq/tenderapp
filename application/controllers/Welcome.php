<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('main');
        $this->load->database();
		$this->load->library('upload');
        $this->load->library('session');

        if($this->session->userdata('Email') == ""){
            echo '<script>window.location.replace("'.base_url.'login")</script>';
        }
	}

	public function index()
	{
	    $data['content'] = 'welcome';
		$this->load->view('masters/template',$data);
	}

    public function view_tender($tender_id)
    {
        $data['item'] = $this->main->get_tender($tender_id)[0];
        $data['content'] = 'view_tender';
        $this->load->view('masters/template',$data);
    }

    public function test()
    {
        $data['content'] = 'test';
        $this->load->view('test',$data);
    }

    public function save_tender()
    {

        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        $ang = $this->input->post('ang');
        $status = $this->input->post('status');
        $wo = $this->input->post('wo');

        $config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = 'true';

		$this->upload->initialize($config);
		if(!$this->upload->do_upload('img')):
			echo $this->upload->display_errors('<p>', '</p>');
        else:
            $img =  upload_url.$this->upload->data('file_name');
            $ar = array('Work_Order_No'=>$wo,'Latitude'=>$lat,'Longitude'=>$lon,'Image'=>$img,'Status'=>$status,'Angles'=>$ang);
            try{
                $this->db->insert('geoRecords',$ar);
            }catch (Exception $e){
                echo $e->getMessage();
            }

            if($this->db->affected_rows() > 0)
                echo "<script>alert('Data Updated');</script>";
            else
                echo "<script>alert('Something went wrong');</script>";
        endif;


        echo "<script>window.location.replace('".base_url."view-tender/".$wo."');</script>";

    }
}
